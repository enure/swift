APP_CONFIG = {
  flickr_user_id:       ENV['SWIFT_FLICKR_USER_ID'],
  flickr_api_key:       ENV['SWIFT_API_KEY'],
  flickr_shared_secret: ENV['SWIFT_FLICKR_SHARED_SECRET'],
  secret_token:         ENV['SECRET_TOKEN'],
  wa_state_tax_url:     'http://dor.wa.gov/AddressRates.aspx',
  personal_blog_url:    'http://cycleswift.wordpress.com/',
  twitter_url:          'http://twitter.com/SwiftIndustries',
  facebook_url:         'http://www.facebook.com/pages/SwiftIndustries/319490293426',
  instagram_url:        'http://instagram.com/swiftindustries/',
  flickr_group_url:     'http://www.flickr.com/groups/swiftindustries/pool/',
  google_analytics_id:  'UA-21808495-1'
}
