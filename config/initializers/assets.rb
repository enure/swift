# If you have other manifests or individual stylesheets and JavaScript files to include,
# you can add them to the precompile array:
Rails.application.config.assets.precompile += [
  'application-hub.css',
  'hub.css',
  'hub.js',
  'print.css',
  'entypo-social.eot',
  'entypo-social.svg',
  'entypo-social.ttf',
  'entypo-social.woff',
  'entypo.eot',
  'entypo.svg',
  'entypo.ttf',
  'entypo.woff',
  'fontello.eot',
  'fontello.svg',
  'fontello.ttf',
  'fontello.woff',
  'flexslider-icon.eot',
  'flexslider-icon.svg',
  'flexslider-icon.ttf',
  'flexslider-icon.woff'
]
