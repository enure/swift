;(function(window, jQuery) {

    var msgBlockList = [
        'Script error.',
        // ReferenceError: Can\'t find ...
        // Assuming these are browser extensions behaving badly
        'variable: cafe',
        'variable: dataKeys',
        'variable: contentSizeInPopover',
        'variable: pageDidLoad',
        'variable: pixelmagsDidLoad',
        'inappbrowser',
        'variable: sCapGloRef',
        'variable: admwl',
        'variable: gcdemodata',
        'variable: atomicFindClose',
        'variable: internalLinks',
        'variable: __gCrWeb',
        'Error calling method on NPObject'
    ],
    scriptUrlBlockList = [
        'assets.pinterest.com/js/pinit.js',
        'google-analytics.com/ga.js',
        'newrelic.com',
        'blockpage.cgi',
        'widgets.pinterest.com'
    ];

    window.onerror = function(msg, url, lineNumber) {
        var string = '',
            m,
            s;

        // Don't bother if there isn't a msg
        if (!msg) { return; }

        if (typeof msg === 'string') {
            for (m = msgBlockList.length - 1; m >= 0; m--) {
                if (msg.indexOf(msgBlockList[m]) !== -1) {
                    return;
                }
            }
        }

        if (url && typeof url === 'string') {
            for (s = scriptUrlBlockList.length - 1; s >= 0; s--) {
                if (url.indexOf(scriptUrlBlockList[s]) !== -1) {
                    return;
                }
            }
        }

        // Don't care about "$.cookie is not a function" exception thrown
        // by Baiduspider
        if (window.navigator.userAgent.indexOf('Baiduspider') !== -1) {
            return;
        }


        string += '== Error\n';
        string += msg + '\n\n';

        if (url) {
            string += '== Script URL\n';
            string += url + '\n\n';
        }

        if (lineNumber) {
            string += '== lineNumber\n';
            string += lineNumber + '\n\n';
        }

        string += '== userAgent\n';
        string += window.navigator.userAgent + '\n\n';

        string += '== URL\n';
        string += window.location.href + '\n\n';


        if (window.location.href.indexOf("swift.dev") !== -1) {
            console.error(string);
        } else {
            jQuery.post("/exceptions/report", { msg: string });
        }

    };
})(window, jQuery);
