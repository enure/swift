SwiftApp.controller('SolsticeCtrl', [
  '$scope',
  '$timeout',
  '$http',
  'ngDialog',
  'ExceptionService',
  'CampoutUserService',
  function($scope, $timeout, $http, ngDialog, ExceptionService, CampoutUserService) {

  'use strict';

  var initializeMap,
      initializeHeader,
      initializeWelcome,
      initializePublicProfiles,
      allMarkers = {},
      publicMarkers = {};

  $scope.changePins = function(pins) {
    if (pins === 'public') {
      $scope.markers = publicMarkers;
    } else {
      $scope.markers = allMarkers;
    }
  };

  $scope.loadCamperProfileInDialog = function(event) {
    var url,
        dialogSettings;

    if (!event.target.getAttribute('url')) {
      return console.error('Could not get profile url');
    }

    url = event.target.getAttribute('url');

    $http
      .get(url)
      .then(function success(response) {
        ngDialog.open({
          template: response.data,
          plain: true
        });
      }, function error(response) {
        ExceptionService.report('Failed to get camper profile for dialog', response);
      });
  };

  initializeMap = function() {
    var mapCenter,
        tilesDict;

    tilesDict = {
          openstreetmap: {
              url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              options: {
                  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              }
          },
          stamenmap: {
              url: "http://tile.stamen.com/watercolor/{z}/{x}/{y}.png",
              options: {
                  attribution: "<a target=_top href=http://maps.stamen.com>Map tiles</a> by <a target=_top href=//stamen.com>Stamen Design</a>, under <a target=_top href=http://creativecommons.org/licenses/by/3.0>CC BY 3.0</a>. Data by <a target=_top href=http://openstreetmap.org>OpenStreetMap</a>, under <a target=_top href=http://creativecommons.org/licenses/by-sa/3.0>CC BY SA</a>."
              }
          }
      };


    if (UA.isMobile()) {
        mapCenter = {
            lat: 42,
            lng: -83,
            zoom: 2
        };
    } else {
        mapCenter = {
            lat: 14,
            lng: -14,
            zoom: 2
        };
    }

    angular.extend($scope, {
      kc: mapCenter,
      tiles: tilesDict.stamenmap,
      defaults: {
        scrollWheelZoom: false
      },
      changeMap: function(map) {
        $scope.tiles = tilesDict[map];
      }
    });

    function populateMap() {
      CampoutUserService
        .getCampoutLocations()
        .success(function(locs) {
          $scope.camperCount = locs.length;

          locs.forEach(function(loc, idx) {
            var msg = "",
                marker;

            msg += "<div>";
            msg += loc.city;

            if (loc.neighbors) {
              msg += "<br>" + loc.neighbors;
              if (loc.neighbors === 1) {
                msg += " neighbor is";
              } else {
                msg += " neighbors are";
              }
              msg += " also attending.";
            }

            if (loc.public_profile) {
              msg += "<br><br>";
              msg += loc.public_contact;
              msg += "<br>";
              msg += "<span ";
              msg +=   "class='S-a' ";
              msg +=   "url='/swiftcampout/profile/" + loc.guid + "' ";
              msg +=   "ng-click='loadCamperProfileInDialog($event)'>";
              msg += "View Profile";
              msg += "</span>";
            }

            msg += "</div>";

            marker = {
              lat: loc.latitude,
              lng: loc.longitude,
              message: msg,
              focus: false,
              draggable: false,
              // this will allow directives to be nested in marker HTML
              getMessageScope: function() { return $scope; }
            };

            allMarkers['m'+idx] = marker;

            if (loc.public_profile) {
              publicMarkers['m'+idx] = marker;
            }

          });

          $scope.markers = allMarkers;
        })
        .error(function(data) {
          ExceptionService.report('Failed to get solstice map data', data);
        });
    }

    $timeout(populateMap, 222);
  };

  initializeHeader = function() {
    var $win = $(window),
        $stickyNav = $('.S-nav'),
        stickyNavTop = $stickyNav.offset().top,
        $stickyLogo = $('.S-logo'),
        stickyLogoTop = $stickyLogo.offset().top,
        stickyLogoHeight = parseFloat($stickyLogo.css('height'));

    $(window)
      .scroll(function(e) {
        if (new Date() % 2 === 0 && !window.hasOwnProperty('ontouchstart')) {
          return;
        }

        setTimeout(function () {
          if ($win.scrollTop() > stickyNavTop) {
            if (!$stickyNav.hasClass('S-nav--fixed')) {
              $stickyNav.addClass('S-nav--fixed');
            }
          } else {
            if ($stickyNav.hasClass('S-nav--fixed')) {
              $stickyNav.removeClass('S-nav--fixed');
            }
          }

          if (!UA.isMobile() && $win.scrollTop() > stickyLogoTop + stickyLogoHeight) {
            if (!$stickyLogo.hasClass('S-logo--fixed')) {
              $stickyLogo.addClass('S-logo--fixed');
            }
          } else {
            if ($stickyLogo.hasClass('S-logo--fixed')) {
              $stickyLogo.removeClass('S-logo--fixed');
            }
          }
        }, 111);
      });
  };

  initializeWelcome = function() {
    var loc = window.location.href;

    if (loc.indexOf('/welcome') === -1) {
      return;
    }

    $scope.newCamper = true;

    $('html, body').animate({
      scrollTop: $('#map').offset().top - 12
    }, 1200);
  };

  initializePublicProfiles = function() {
    CampoutUserService
      .getCamperProfiles()
      .success(function(data, status) {
        $scope.publicProfiles = data;
      })
      .error(function(response) {
        console.error(response);
      });
  };

  initializeMap();

  $(window).on('load', function() {
    initializeHeader();
    initializeWelcome();
    initializePublicProfiles();
  });
}]);
