class Size < ActiveRecord::Base

  belongs_to :product, touch: true
  validates_presence_of :title, :price, :wholesale_price

  PRICE_MATCH = /\A\d{0,10}\.\d{2}\z/
  PRICE_MESSAGE = "must be in the following format: 12.00"
  validates_format_of :price, :with => PRICE_MATCH, :message => PRICE_MESSAGE, :if => :price?
  validates_format_of :wholesale_price, :with => PRICE_MATCH, :message => PRICE_MESSAGE, :if => :wholesale_price?

  # Not used
  # def price_for is_wholesale_user
  #     is_wholesale_user ? self.wholesale_price : self.price
  # end

end
