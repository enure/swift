var oneDollarShippingCart = {
    "price": 1800,
    "total": 1800,
    "products": [{
        "parts": [],
        "id": 79,
        "title": "Celestial Kerchief",
        "price": 9,
        "totalPrice": 9,
        "answer": "",
        "kind": "Accessory",
        "question": "",
        "width": 6,
        "height": 6,
        "length": 1,
        "weight": 0.12,
        "package_type": "CUSTOM",
        "domestic_flat_rate_shipping_charge": 100,
        "international_flat_rate_shipping_charge": 200,
        "quantity": 1,
        "uniqueId": 1413254245877
    }, {
        "parts": [],
        "id": 79,
        "title": "Celestial Kerchief",
        "price": 9,
        "totalPrice": 9,
        "answer": "",
        "kind": "Accessory",
        "question": "",
        "width": 6,
        "height": 6,
        "length": 1,
        "weight": 0.12,
        "package_type": "CUSTOM",
        "domestic_flat_rate_shipping_charge": 100,
        "international_flat_rate_shipping_charge": 200,
        "quantity": 1,
        "uniqueId": 1413254917654
    }]
};
var veryHeavyShippingCart = {
    "price": 1800,
    "total": 1800,
    "products": [{
        "parts": [],
        "id": 79,
        "title": "Celestial Kerchief",
        "price": 9,
        "totalPrice": 9,
        "answer": "",
        "kind": "Accessory",
        "question": "",
        "width": 6,
        "height": 6,
        "length": 6,
        "weight": 40,
        "package_type": "CUSTOM",
        "domestic_flat_rate_shipping_charge": null,
        "international_flat_rate_shipping_charge": null,
        "quantity": 1,
        "uniqueId": 1413254245870
    }]
};
var manyDollarShippingCart = {
    "price": 30000,
    "total": 30000,
    "products": [{
        "parts": [{
            "id": 39,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Body"
        }, {
            "id": 40,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Lid"
        }, {
            "id": 42,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Rear Pocket"
        }, {
            "id": 44,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Thread"
        }, {
            "id": 96,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Trim"
        }, {
            "id": 36,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Front Pocket"
        }],
        "id": 2,
        "title": "Short Stack Panniers",
        "price": 300,
        "totalPrice": 300,
        "answer": "",
        "kind": "Product",
        "question": "",
        "width": 16,
        "height": 13,
        "length": 7,
        "weight": 5.2,
        "package_type": "CUSTOM",
        "domestic_flat_rate_shipping_charge": null,
        "international_flat_rate_shipping_charge": null,
        "quantity": 1,
        "uniqueId": 1413259261984
    }]
};
var manyDollarMultiItemShippingCart = {
    "price": 30900,
    "total": 30900,
    "products": [{
        "parts": [{
            "id": 39,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Body"
        }, {
            "id": 40,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Lid"
        }, {
            "id": 42,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Rear Pocket"
        }, {
            "id": 44,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Thread"
        }, {
            "id": 96,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Trim"
        }, {
            "id": 36,
            "price": null,
            "selectedColor": {
                "hex": "#000",
                "id": 1,
                "price": null,
                "title": "Black",
                "wholesale_price": null
            },
            "title": "Front Pocket"
        }],
        "id": 2,
        "title": "Short Stack Panniers",
        "price": 300,
        "totalPrice": 300,
        "answer": "",
        "kind": "Product",
        "question": "",
        "width": 16,
        "height": 13,
        "length": 7,
        "weight": 5.2,
        "package_type": "CUSTOM",
        "domestic_flat_rate_shipping_charge": null,
        "international_flat_rate_shipping_charge": null,
        "quantity": 1,
        "uniqueId": 1413259261984
    }, {
        "parts": [],
        "id": 79,
        "title": "Celestial Kerchief",
        "price": 9,
        "totalPrice": 9,
        "answer": "",
        "kind": "Accessory",
        "question": "",
        "width": 6,
        "height": 6,
        "length": 1,
        "weight": 0.12,
        "package_type": "PAK",
        "domestic_flat_rate_shipping_charge": 200,
        "international_flat_rate_shipping_charge": 200,
        "quantity": 1,
        "uniqueId": 1413340302632
    }]
};
var domesticShippingFormValues = {
    '#js-input-pickup':  false,
    '#js-input-country': 'US',
    '#js-input-line1': '425 E Sussex AVE',
    '#js-input-city': 'Missoula',
    '#js-input-state': 'MT',
    '#js-input-zip': '59801',
    '#js-input-phone': '406 219 1062',
    '#js-input-shipping-contact': 'Nobody But Me'
};
var intlShippingFormValues = {
    '#js-input-pickup':  false,
    '#js-input-country': 'CA',
    '#js-input-line1': '750 Hornby Street',
    '#js-input-city': 'Vancouver',
    '#js-input-state': 'BC',
    '#js-input-zip': 'V6Z 2H7',
    '#js-input-phone': '604.662.4700',
    '#js-input-shipping-contact': 'Nobody But Me'
};

var noop = function(){};

casper.test.begin('Testing Shipping', function suite(test) {

    casper.on('remote.message', function(msg) {
        casper.echo(msg);
    });

    casper.start('http://localhost:3000/cart/checkout', function() {
        this.evaluate(function(oneDollarShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(oneDollarShippingCart));
        }, oneDollarShippingCart);
        this.reload(noop);
    });

    casper.then(function() {
        this.echo(this.getCurrentUrl());
        this.waitForSelector('#js-input-pickup', noop);
    });

    // Domestic shipping
    casper.then(function() {
        this.fillSelectors('#js-shipping-form', domesticShippingFormValues, false);
        this.echo('=> Getting rates...');
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        this.test.assert(bestPrice === 'USPS:GROUND:100', 'Shipping is USPS:GROUND:100');
    });

    casper.then(function() {
       var shippingServiceOptions = this.evaluate(function() {
           return jQuery('#js-domestic-shipping-service-levels').val();
       });
       this.test.assert(shippingServiceOptions === 'GROUND');
    });

    casper.then(function() {
       var shippingServiceOptionsLength = this.evaluate(function() {
           return document.getElementById('js-domestic-shipping-service-levels').options.length;
       });
       this.test.assert(shippingServiceOptionsLength === 1);
    });

    // Flat Rate INTL Shipping
    casper.then(function() {
        this.echo('=> Updating cart...');
        this.evaluate(function(oneDollarShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(oneDollarShippingCart));
        }, oneDollarShippingCart);

        this.reload(noop);
    });

    casper.then(function() {
        this.echo('=> Getting rates...');
        this.fillSelectors('#js-shipping-form', intlShippingFormValues, false);
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        var priceParts = bestPrice.split(':');

        this.test.assert(priceParts[0] === 'USPS', 'Best provider should be USPS');
        this.test.assert(priceParts[1] === 'INTL_SURFACE', 'Best service should be INTL_SURFACE');
        this.test.assert(parseFloat(priceParts[2]) === 200, 'Best price should be 2 dollars');
    });

    casper.then(function() {
       var shippingServiceOptions = this.evaluate(function() {
           return jQuery('#js-intl-shipping-service-levels').val();
       });
       this.test.assert(shippingServiceOptions === 'INTL_SURFACE');
    });

    casper.then(function() {
       var shippingServiceOptionsLength = this.evaluate(function() {
           return document.getElementById('js-intl-shipping-service-levels').options.length;
       });
       this.test.assert(shippingServiceOptionsLength === 1);
    });

    // Many dollar, multi-item domestic
    casper.then(function() {
        this.echo('=> Updating cart...');
        this.evaluate(function(manyDollarMultiItemShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(manyDollarMultiItemShippingCart));
        }, manyDollarMultiItemShippingCart);

        this.reload(noop);
    });

    casper.then(function() {
        this.echo('=> Getting rates...');
        this.fillSelectors('#js-shipping-form', domesticShippingFormValues, false);
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        var priceParts = bestPrice.split(':');

        this.test.assert(priceParts[0] === 'ups', 'Best provider should be UPS');
        this.test.assert(priceParts[1] === 'GROUND', 'Best services should be GROUND');
        this.test.assert(parseFloat(priceParts[2]) > 900 && parseFloat(priceParts[2]) < 1100, 'Best price should be between 9 and 11 dollars');
    });

    casper.then(function() {
        this.echo('=> Updating cart...');
        this.evaluate(function(manyDollarShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(manyDollarShippingCart));
        }, manyDollarShippingCart);

        this.reload(noop);
    });

    casper.then(function() {
        this.echo('=> Getting rates...');
        this.fillSelectors('#js-shipping-form', domesticShippingFormValues, false);
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        var priceParts = bestPrice.split(':');

        this.test.assert(priceParts[0] === 'ups', 'Best provider should be UPS');
        this.test.assert(priceParts[1] === 'GROUND', 'Best services should be GROUND');
        this.test.assert(parseFloat(priceParts[2]) > 900 && parseFloat(priceParts[2]) < 1100, 'Best price should be between 9 and 11 dollars');
    });

    casper.then(function() {
        this.echo('=> Updating cart...');
        this.evaluate(function(manyDollarMultiItemShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(manyDollarMultiItemShippingCart));
        }, manyDollarMultiItemShippingCart);

        this.reload(noop);
    });

    casper.then(function() {
        this.echo('=> Getting rates...');
        this.fillSelectors('#js-shipping-form', domesticShippingFormValues, false);
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        var priceParts = bestPrice.split(':');

        this.test.assert(priceParts[0] === 'ups', 'Best provider should be UPS');
        this.test.assert(priceParts[1] === 'GROUND', 'Best services should be GROUND');
        this.test.assert(parseFloat(priceParts[2]) > 900 && parseFloat(priceParts[2]) < 1100, 'Best price should be between 9 and 11 dollars');
    });

    casper.then(function() {
        this.echo('=> Updating cart...');
        this.evaluate(function(veryHeavyShippingCart) {
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(veryHeavyShippingCart));
        }, veryHeavyShippingCart);

        this.reload(noop);
    });

    casper.then(function() {
        this.echo('=> Getting rates...');
        this.fillSelectors('#js-shipping-form', domesticShippingFormValues, false);
        this.click('#js-rates-submit');
        this.waitUntilVisible('#js-best-price-true', noop);
    });

    casper.then(function() {
        this.echo('=> Evaluating rates...');
        var bestPrice = this.evaluate(function() {
            return jQuery('#js-best-price-true').val();
        });
        this.echo('=> Shipping ' + bestPrice);
        var priceParts = bestPrice.split(':');

        this.test.assert(priceParts[0] === 'ups', 'Best provider should be UPS');
        this.test.assert(priceParts[1] === 'GROUND', 'Best services should be GROUND');
        this.test.assert(parseFloat(priceParts[2]) > 2000 && parseFloat(priceParts[2]) < 2200, 'Best price should be between 20 and 22 dollars');
    });

    casper.run(function() {
        test.done();
    });

});
